# Systemd Removed Services Hook

Notifies you of any service which was enabled, but no longer exists because the package for the service was uninstalled.
